document.addEventListener('DOMContentLoaded', function () {
	var mymap = L.map('map',{zoomControl: false}).setView([53.86, 27.53], 10);
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
		maxZoom: 18,
		attribution: 'zzz',
		id: 'mapbox.streets'
	}).addTo(mymap);


	function onMapClick(e) {
		console.log("You clicked the map at " + e.latlng.toString())
	}

	mymap.on('click', onMapClick);

	L.marker([53.86, 27.53]).addTo(mymap).bindPopup('A pretty CSS3 popup.<br> Easily customizable.');

	$(function() { new Yabedawdg({key: "r2qg2V3Zdjq9i5s046Pb", block: '.recall__body', address: 'Минск, улица Максима Богдановича, 6', name: '12maps', category: 'Maps'}).draw(); })

});

$('.sidebar__category-preview').click(function(){
	$(this).addClass('_active').siblings().removeClass('_active');
	$('.sidebar__category-body').removeClass('_active').eq($(this).index()).addClass('_active');
	$('.sidebar__body').toggleClass('_active');
	$('.sidebar__head').toggleClass('_active');
});
$('.authorization__singin-button').click(function(){
	$(this).parent().toggleClass('_active');
	$(this).parent().parent().toggleClass('_active');
});
$('.popover__action-button--minus').click(function(){
	$(this).parent().parent().parent().toggleClass('_active');
});
$('.popover__action-button--cross').click(function(){
	$(this).parent().parent().parent().parent().removeClass('_active');
	$('.sidebar').removeClass('_active');
});
$('.popover--singin .popover__action-button--cross').click(function(){
	$('.authorization__singin').removeClass('_active');
	$('.authorization').removeClass('_active');
});
$('.card__button--minus').click(function(){
	$('.card__body').toggleClass('_active');
});
$('.card__button--cross').click(function(){
	$('.card').toggleClass('_active');
});
$('.places__cat-plus').click(function(){
	$(this).parent().toggleClass('_active');
});
$('.route__link-button').click(function(){
	$(this).parent().toggleClass('_active');
});
$('.places__cat-title').click(function(){
	$(this).toggleClass('_selected');
});
$('.places__cat-link').click(function(){
	$(this).toggleClass('_selected');
});
$('.header__button--search').click(function(){
	$('.search').toggleClass('_active');
	$('.sidebar__body').removeClass('_active');
	$('.sidebar__head').removeClass('_active');
});
$('.header__button--auth').click(function(){
	$('.authorization').toggleClass('_active');
});
$('.header__menu').click(function(){
	$(this).toggleClass('_active');
	$('.sidebar__body').toggleClass('_active');
	$('.sidebar__head').toggleClass('_active');
	$('.sidebar').toggleClass('_active');
});
$('.footer__lang--en').click(function(){
	$('.g-doc').toggleClass('_en');
});
$('.footer__lang--ar').click(function(){
	$('.g-doc').toggleClass('_ar');
});

!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);

$('.point').draggable();
$( ".route__body" ).sortable();
// $( ".route__body" ).disableSelection();


$( ".route__pointicon" ).draggable({
	revert: true,
	revertDuration: 500,
	drag: function( event, ui ) {
	$( this )
		.addClass( "_active" );
	},
	stop: function( event, ui ) {
	$( this )
		.removeClass( "_active" );
	}
});


var page = $('.g-page'),
	pageWidth = page.width(),
	popup = {
		activeClass: 'open',
		open: function (popupName) {
			page.addClass('lock').css({ 'margin-right': page.width() - pageWidth });
			document.querySelector('[data-popup="' + popupName + '"]').classList.add(this.activeClass);
		},
		openByHash: function () {
			if (location.hash != '' && location.hash.indexOf('#popup/') >= 0) {
				this.open(location.hash.replace('#popup/', ''));
			}
		},
		close: function () {
			page.removeClass('lock').css({ 'margin-right': '' });
			$('.popup').removeClass(this.activeClass);
			history.replaceState('', '', location.pathname);
		}
	};

window.addEventListener('load', function (e) {
	popup.openByHash();
});
window.addEventListener('hashchange', function (e) {
	popup.openByHash();
});

$('.popup__close,.popup__overlay').click(function () {
	popup.close();
});
